'use strict';

import User from './user.model';
import config from '../../config/environment';
import jwt from 'jsonwebtoken';

function validationError(res, statusCode) {
  statusCode = statusCode || 422;
  return function(err) {
    return res.status(statusCode).json(err);
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    console.error(err);
    return res.status(statusCode).send(err);
  };
}

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

/**
 * Get list of students
 * restriction: 'admin'
 */
export function index(req, res) {
  return User.find({ role: 'student' }, '-salt -password').exec()
    .then(users => {
      res.status(200).json(users);
      return null;
    })
    .catch(handleError(res));
}

/**
 * Get list of admin users
 * restriction: 'admin'
 */
export function admins(req, res) {
  return User.find({ role: 'admin' }, '-salt -password').exec()
    .then(users => {
      res.status(200).json(users);
      return null;
    })
    .catch(handleError(res));
}

/**
 * Creates a new user
 */
export function create(req, res) {
  var newUser = new User(req.body);
  newUser.provider = 'local';
  // newUser.role = 'student';
  newUser.save()
    .then(function(user) {
      var token = jwt.sign({ _id: user._id }, config.secrets.session, {
        expiresIn: 60 * 60 * 30
      });
      res.json({
        id: user._id,
        token: token
      });
    })
    .catch(validationError(res));
}

/**
 * Get a single user
 */
export function show(req, res, next) {
  var userId = req.params.id;

  return User.findById(userId).exec()
    .then(user => {
      if(!user) {
        return res.status(404).end();
      }
      res.json(user.profile);
    })
    .catch(err => next(err));
}

/**
 * Deletes a user
 * restriction: 'admin'
 */
export function destroy(req, res) {
  return User.findByIdAndRemove(req.params.id).exec()
    .then(function() {
      res.status(204).end();
      return null;
    })
    .catch(handleError(res));
}

/**
 * Change a users password
 */
export function changePassword(req, res) {
  var userId = req.user._id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  return User.findById(userId).exec()
    .then(user => {
      if(user.authenticate(oldPass)) {
        user.password = newPass;
        return user.save()
          .then(() => {
            res.status(204).end();
          })
          .catch(validationError(res));
      } else {
        return res.status(403).end();
      }
    });
}

/**
 * Get my info
 */
export function me(req, res, next) {
  var userId = req.user._id;

  return User.findOne({ _id: userId }, '-salt -password').exec()
    .then(user => {
      if(!user) {
        return res.status(401).end();
      }
      res.json(user);
      return null;
    })
    .catch(err => next(err));
}

/**
 * Notify if student is present
 */
export function studentDetected(req, res, next) {
  if(req.body._id) {
    delete req.body._id;
  }

  return User.findOne({ _id: req.params.id }, '-salt -password').exec()
    .then(user => {
      if(!user) {
        return res.status(401).end();
      }

      user.inClass = true;

      return user.save()
        .then(() => {
          res.status(204).end();
          return null;
        })
        .catch(validationError(res));
    })
    .catch(err => {
      console.error(err);
      next(err);
    });
}

/**
 * Notify if student has left
 */
export function studentLeft(req, res, next) {
  if(req.body._id) {
    delete req.body._id;
  }

  return User.findOne({ _id: req.params.id }, '-salt -password').exec()
    .then(user => {
      if(!user) {
        return res.status(401).end();
      }

      user.inClass = false;

      return user.save()
        .then(() => {
          res.status(204).end();
          return null;
        })
        .catch(validationError(res));
    })
    .catch(err => next(err));
}

/**
 * Notify student with a <ding>
 */
export function studentDing(req, res, next) {
  if(req.body._id) {
    delete req.body._id;
  }

  return User.findOne({ _id: req.params.id }, '-salt -password').exec()
    .then(user => {
      if(!user) {
        return res.status(401).end();
      }

      user.warnings++;

      return user.save()
        .then(() => {
          res.status(204).end();
          return null;
        })
        .catch(validationError(res));
    })
    .catch(err => {
      console.error(err);
      next(err);
    });
}

export function upsert(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }

  return User.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true, upsert: true, setDefaultsOnInsert: true }).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

/**
 * Authentication callback
 */
export function authCallback(req, res) {
  res.redirect('/');
}

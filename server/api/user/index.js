'use strict';

import {Router} from 'express';
import * as controller from './user.controller';
import * as auth from '../../auth/auth.service';

var router = new Router();

router.get('/', controller.index);
router.post('/', controller.create);
router.get('/:id/detected', controller.studentDetected);
router.get('/:id/left', controller.studentLeft);
router.get('/:id/ding', controller.studentDing);

router.get('/me', auth.isAuthenticated(), controller.me);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.put('/:id/password', auth.isAuthenticated(), controller.changePassword);

router.get('/admins', auth.hasRole('admin'), controller.admins);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);
router.put('/:id', auth.hasRole('admin'), controller.upsert);

module.exports = router;

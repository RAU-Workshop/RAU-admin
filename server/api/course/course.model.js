'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './course.events';

var CourseSchema = new mongoose.Schema({
  name: { type: String, required: true, trim: true },
  date: { type: Date, default: Date.now, required: true },
  started: { type: Boolean, default: false },
  ended: { type: Boolean, default: false }
});

registerEvents(CourseSchema);
export default mongoose.model('Course', CourseSchema);

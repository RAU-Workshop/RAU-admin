/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
import User from '../api/user/user.model';
import Course from '../api/course/course.model';
import config from './environment/';

export default function seedDatabaseIfNeeded() {
  if(config.seedDB) {
    User.find({}).remove()
      .then(() => {
        User.create({
          provider: 'local',
          role: 'admin',
          name: 'Marius',
          email: 'marius.stuparu@oracle.com',
          password: 'marius'
        }, {
          provider: 'local',
          role: 'admin',
          name: 'Mihai',
          email: 'mihai.croitoru@oracle.com',
          password: 'mihai'
        }, {
          provider: 'local',
          role: 'admin',
          name: 'Teacher',
          email: 'pi@rau.bzz.click',
          password: 'teacher'
        }, {
          provider: 'local',
          name: 'Jayden Nash',
          email: 'JaydenNash@armyspy.com',
          imageURL: 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/13/1342ef11e51decd6220c47de7789db8b2f7a1037_full.jpg',
          password: 'Ung0rohzoh',
          presence: [
            {
              date: new Date('2017-04-26'),
              present: true
            }, {
              date: new Date('2017-05-03'),
              present: true
            }, {
              date: new Date('2017-05-10'),
              present: false
            }, {
              date: new Date('2017-05-17'),
              present: true
            },
          ]
        }, {
          provider: 'local',
          name: 'Shannon Weston',
          email: 'ShannonWeston@teleworm.us',
          imageURL: 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/11/11fdaa41e55cf5e3347f11e673ac27ea1ce5815e_full.jpg',
          password: 'Ioj2oinee',
          presence: [
            {
              date: new Date('2017-04-26'),
              present: false
            }, {
              date: new Date('2017-05-03'),
              present: true
            }, {
              date: new Date('2017-05-10'),
              present: true
            }, {
              date: new Date('2017-05-17'),
              present: true
            },
          ]
        }, {
          provider: 'local',
          name: 'Scarlett Bibi',
          email: 'ScarlettBibi@armyspy.com',
          imageURL: 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/3b/3b67f50d57fb855fdeba176b0b3680fb5ea292af_full.jpg',
          password: 'Vie6aech',
          presence: [
            {
              date: new Date('2017-04-26'),
              present: true
            }, {
              date: new Date('2017-05-03'),
              present: true
            }, {
              date: new Date('2017-05-10'),
              present: false
            }, {
              date: new Date('2017-05-17'),
              present: true
            },
          ]
        }, {
          provider: 'local',
          name: 'Ryan Powell',
          email: 'RyanPowell@teleworm.us',
          imageURL: 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/a2/a297ce1afd3de17b00ed9d55e86b55cbfbe715d2_full.jpg',
          password: 'Phai3aik8',
          presence: [
            {
              date: new Date('2017-04-26'),
              present: true
            }, {
              date: new Date('2017-05-03'),
              present: false
            }, {
              date: new Date('2017-05-10'),
              present: true
            }, {
              date: new Date('2017-05-17'),
              present: true
            },
          ]
        })
        .then(() => console.log('finished populating users'))
        .catch(err => console.log('error populating users', err));
      });
    Course.find({}).remove()
      .then(() => {
        Course.create({
          name: 'Sesiunea 1',
          date: new Date('2017-04-26'),
          started: false,
          ended: true
        }, {
          name: 'Sesiunea 2',
          date: new Date('2017-05-03'),
          started: false,
          ended: true
        }, {
          name: 'Sesiunea 3',
          date: new Date('2017-05-10'),
          started: false,
          ended: true
        }, {
          name: 'Sesiunea 4',
          date: new Date('2017-05-17'),
          started: false,
          ended: true
        }, {
          name: 'Sesiunea 5',
          date: new Date('2017-05-24'),
          started: false,
          ended: false
        })
        .then(() => console.log('finished populating courses'))
        .catch(err => console.log('error populating courses', err));
      });
  }
}

'use strict';
/*eslint no-process-env:0*/

// Development specific configuration
// ==================================
module.exports = {

  // MongoDB connection options

  // Use this version if you installed MongoDB as a stand-alone server
  // mongo: {
  //   uri: 'mongodb://localhost:27017/rauworkshop-dev'
  // },

  // Use this version if you followed my readme and created a Docker container for MongoDB
  mongo: {
    uri: 'mongodb://localhost:27018/rauworkshop-dev'
  },

  // Seed database on startup
  seedDB: true

};

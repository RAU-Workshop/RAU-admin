'use strict';
const angular = require('angular');

/*@ngInject*/
export function coursesService($resource) {
  return $resource('/api/courses/:id/:controller', {
    id: '@_id'
  }, {
    get: {
      method: 'GET',
      params: {
        id: 'id'
      }
    },
    save: {
      method: 'PUT'
    }
  });
}


export default angular.module('rauworkshopApp.courses', [])
  .factory('Courses', coursesService)
  .name;

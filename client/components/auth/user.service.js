'use strict';

export function UserResource($resource) {
  'ngInject';

  return $resource('/api/users/:id/:controller', {
    id: '@_id'
  }, {
    changePassword: {
      method: 'PUT',
      params: {
        controller: 'password'
      }
    },
    get: {
      method: 'GET',
      params: {
        id: 'me'
      }
    },
    save: {
      method: 'PUT'
    },
    admins: {
      method: 'GET',
      isArray: true,
      params: {
        controller: 'admins'
      }
    },
    ding: {
      method: 'GET',
      params: {
        id: 'id',
        controller: 'ding'
      }
    },
  });
}

'use strict';
// @flow

export default class AdminController {
  students: Object[];
  sessions: Object[];
  socket;
  currentDate: Date;

  /*@ngInject*/
  constructor(User, Courses, socket, $scope) {
    // Register global variables
    this.Courses = Courses;
    this.User = User;
    this.socket = socket;
    this.students = [];
    this.sessions = [];

    // Stop watching the socket if $scope is destroyed
    $scope.$on('$destroy', () => {
      socket.unsyncUpdates('course');
      socket.unsyncUpdates('student');
    });
  }

  $onInit() {
    this.currentDate = new Date();

    // Fetch all course data, make sure all dates are compliant
    // then monitor socketio for changes
    this.Courses.query(response => {
      this.sessions = response;

      this.socket.syncUpdates('course', this.sessions);
    });

    // Fetch all student data, then monitor socketio for changes
    this.User.query(response => {
      this.students = response;
      this.socket.syncUpdates('student', this.students);
    });
  }

  // Remove student from database
  unEnroll(student) {
    student.$remove(() => {
      this.students.splice(this.students.indexOf(student), 1);
    });
  }

  isToday(courseDate) {
    let testDate = new Date(courseDate);
    return testDate.toDateString() == this.currentDate.toDateString();
  }

  hasStarted(course) {
    return course.started;
  }

  startCourse(course) {
    course.started = true;
    course.ended = false;
    course.$save(() => {
      console.log(`Course "${course.name}" has STARTED`);
    });
  }

  endCourse(course) {
    let today = new Date();

    course.started = false;
    course.ended = true;
    course.$save(() => {
      console.log(`Course "${course.name}" has ENDED`);
      console.log('Updating all students presence now...');
      for (let student of this.students) {
        student.presence.push({
          date: today,
          present: student.inClass
        });

        this.User.save({ id: student._id }, student);
      }
    });
  }

  sendWarning(student) {
    this.User.ding({ id: student._id });
  }
}

# RAU workshop

## Admin area

Administration / teacher area for the IoT Workshop @ RAU by Oracle

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a>

Latest build from 2017-07-13 is live on Heroku at this link: [rau.bzz.click](http://rau.bzz.click/).

This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.

## Getting Started

### Prerequisites

- [Git](https://git-scm.com/) - public repo on Gitlab @ [bzz.click/rauadmin](http://bzz.click/rauadmin)
- [Node.js and npm](https://nodejs.org) Node >= 6.x.x, npm >= 2.x.x
- [Gulp](http://gulpjs.com/) (`npm install --global gulp`)
- [MongoDB Community](https://www.mongodb.com/download-center?jmp=nav#community) - Keep a running daemon with `mongod`

### Recommended (all)

- Editors: [Visual Studio Code](https://code.visualstudio.com) or [Atom](https://atom.io/)
- Other Node.js tools: Yeoman and Angular Fullstack Generator `npm i -g yo generator-angular-fullstack`
- Git UI apps: [GitKraken](https://www.gitkraken.com/)
- [Docker CE](https://docker.com) (requires Windows 10 or MacOS X El Capitan 10.11)

### Required for Windows users

You need to install some helper build tools for some packages to install correctly. Please follow the [instructions here](https://github.com/nodejs/node-gyp#installation).

Fastest and easiest way to fix the Windows modules problems:

- Install "windows-build-tools", as an administrtor user, with `npm install --global --production windows-build-tools`
- Download and install [Python 2.7](https://www.python.org/downloads/) (not 3.3!) and **wait for it to finish**
- Issue a `npm config set msvs_version 2015` command
- Tell node-gyp where the Python executable is, with `node-gyp --python D:\\Python27\\python.exe` (make sure you use the path where YOU installed Python)
- Optional, if you already ran `npm install` and had errors, simply run `npm install node-zopfli` (this module was causing the errors)

### Developing

1. Run `npm install` to install server dependencies.

2. Run `mongod` in a separate shell to keep an instance of the MongoDB Daemon running. You can follow [this guide](http://www.mkyong.com/mongodb/how-to-install-mongodb-on-windows/) to install MongoDB as a Windows service.

3. Run `gulp serve` to start the development server. It should automatically open the client in your browser when ready.

## Build & development

Start the MongoDB Docker container:

#### First time
- `docker pull mongo:latest`
- `docker run -v "$(pwd)":/data --name rauworkshop -p 27017:27018 -d mongo mongod --smallfiles`

#### Subsequent runs
```
docker start `docker ps -a -q --filter "name=rauworkshop"`
```

#### After MongoDB is started
Run `gulp build` for building or `gulp serve` for preview.

## Testing

Running `npm test` will run the unit tests with karma.

## Presentations used during workshops

- [Session 1 presentation](https://www.slideshare.net/secret/Kz0chnHKwZbBn0)
- [Session 2 presentation](https://www.slideshare.net/secret/LsiNOgaKFCCUdI)
